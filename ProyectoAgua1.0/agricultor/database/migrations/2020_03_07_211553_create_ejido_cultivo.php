<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEjidoCultivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ejido_cultivo', function (Blueprint $table) {
        Schema::disableForeignKeyConstraints();
$table->Increments("Id_ejido-cultivo");
$table->unsignedInteger("Id_Ejido");
$table->unsignedInteger("Id_cultivo");
$table->date("fecha_siembra");
$table->date("fecha_cosecha");
$table->date("fecha_retsoca");
$table->boolean("pozo");
$table->string("Estado",50);
$table->float("Superficie_Riego");
$table->foreign('Id_Ejido')->references('Id_Ejido')->on('ejido');
$table->foreign('Id_cultivo')->references('Id_cultivo')->on('cultivo');
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('ejido_cultivo');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
