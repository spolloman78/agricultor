<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEjidoConcepto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('ejido_concepto', function (Blueprint $table) {
   Schema::disableForeignKeyConstraints();
  $table->Increments('Id_ejido-concepto');
  $table->string("ciclo",10);
  $table->unsignedInteger("Id_ejido-cultivo");
  $table->foreign('Id_ejido-cultivo')->references('Id_ejido-cultivo')->on('ejido_cultivo');


});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('ejido_concepto');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
