<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario', function (Blueprint $table) {
        Schema::disableForeignKeyConstraints();
            $table->increments("Id_usuario");
            $table->string('nombre');
            $table->string('ap-Paterno');
            $table->string('ap-Materno');
            $table->unsignedInteger('Id_rol');
            $table->string('usuario')->unique();
            $table->foreign('Id_rol')->references('Id_rol')->on('roles');
          //  $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('usuario');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
