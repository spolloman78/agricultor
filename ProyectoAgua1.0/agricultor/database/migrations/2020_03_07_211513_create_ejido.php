<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEjido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ejido', function (Blueprint $table) {
          Schema::disableForeignKeyConstraints();
           $table->Increments("Id_Ejido");

           $table->String("Id_ejidatario");
           $table->unsignedInteger("Id_municipio");
           $table->unsignedInteger("Id_canalero");
           $table->string("nombre_Ejido",150);
           $table->integer("SEC");
           $table->integer("CP");
           $table->integer("LT");
           $table->integer("SLT");
           $table->integer("RA");
           $table->integer("PC");
           $table->integer("TE");
           $table->integer("SR");
           $table->integer("EQ");
           $table->float("sup-Fis");
           $table->string("DOC",100);
           $table->foreign('Id_canalero')->references('Id_canalero')->on('canalero');
           $table->foreign("Id_municipio")->references("Id_municipio")->on('municipio');
           $table->foreign('Id_ejidatario')->references('Id_ejidatario')->on('ejidatario');

          //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {    DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('ejido');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
