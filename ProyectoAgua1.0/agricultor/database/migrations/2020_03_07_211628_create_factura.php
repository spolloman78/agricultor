<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactura extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura', function (Blueprint $table) {
           Schema::disableForeignKeyConstraints();
            $table->string("Id_factura",30);
            $table->primary("Id_factura");
            $table->double("total");
            $table->integer("status-fact");
            $table->String("Id_ejidatario",25);
            $table->unsignedInteger('Id_ejido-concepto');
            $table->foreign('Id_ejidatario')->references('Id_ejidatario')->on('ejidatario');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('factura');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
