<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

         $this->call(roles::class);
         $this->call(Tenencia::class);
         $this->call(Municipio::class);
         $this->call(Canalero::class);
    }
}
