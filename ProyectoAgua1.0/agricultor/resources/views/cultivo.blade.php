@extends('layouts.layout')

@section('content')
<div class="main-panel">
    <div class="content">
      <div class="page-inner">
        <div class="page-header">
          <h4 class="page-title">Cultivos</h4>
          <ul class="breadcrumbs">
            <li class="nav-home">
              <a href="#">
                <i class="flaticon-home"></i>
              </a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Catalogos</a>
            </li>
            <li class="separator">
              <i class="flaticon-right-arrow"></i>
            </li>
            <li class="nav-item">
              <a href="#">Cultivos</a>
            </li>
          </ul>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Catalogo de Cultivos  19/20   <i class="fas fa-tree"></i></h4>


              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="basic-datatables" class="display table table-striped table-hover" >
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Nombre del Cultivo</th>
                        <th>Riegos Totales</th>
                        <th>Riegos disponibles</th>
                        <th>Prioridad</th>

                      </tr>
                    </thead>
<script>
var contador=0;
var cap=0;
</script>
                    <tbody>


                      @foreach($cultivo as $row)


                      <tr>
                        <td>{!!$row->Id_cultivo!!}</td>
                        <td>{!!$row->descripcion!!}</td>
                        <td id="capacidad">{!!$row->capacidad_cultivo!!}</td>

                    <td id="disponibles">{!!$row->cantidad_cultivo!!}
                      @php
$valor=($row->cantidad_cultivo+0)/($row->capacidad_cultivo)*100;
@endphp
<script>
contador++;
var g=document.getElementById("basic-datatables");
var capacidad=g.rows[contador].cells[2].innerText;
var disponibles=g.rows[contador].cells[3].innerText;
cap=(disponibles/capacidad)*100;
var disponibles=g.rows[contador].cells[3].innerText=cap+" %";
//document.getElementsByClassName("progress-bar")[0].style.width = cap+"%";

</script>
<div class="progress">
  <div class="progress-bar progress-bar-striped bg-success" role="progressbar" id="porcentaje" @php echo "style='width:".  $valor."%'"; @endphp aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
</div>

                        </td>
                        <td>{!!$row->prioridad!!}</td>

                      </tr>

    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          @endsection
